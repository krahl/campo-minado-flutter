import 'package:campominado/components/resultado_widget.dart';
import 'package:campominado/components/tabuleiro_widget.dart';
import 'package:campominado/models/campo.dart';
import 'package:campominado/models/explosao_exception.dart';
import 'package:campominado/models/tabuleiro.dart';
import 'package:campominado/utils/myColors.dart';
import 'package:flutter/material.dart';

class CampoMinadoApp extends StatefulWidget {
  @override
  _CampoMinadoAppState createState() => _CampoMinadoAppState();
}

class _CampoMinadoAppState extends State<CampoMinadoApp> {
  bool _venceu;
  Tabuleiro _tabuleiro;
  int _qtdBombas = 3;

  void _reiniciar() {
    setState(() {
      _venceu = null;
      _tabuleiro.reiniciar();
    });
  }

  void _abrir(Campo campo) {
    if (_venceu != null) return;

    setState(() {
      try {
        campo.abrir();
        if (_tabuleiro.resolvido) {
          _venceu = true;
        }
      } on ExplosaoException {
        _venceu = false;
        _tabuleiro.revelarBombas();
      }
    });
  }

  void _alternarMarcacao(Campo campo) {
    if (_venceu != null) return;

    setState(() {
      campo.alternarMarcacao();
      if (_tabuleiro.resolvido) {
        _venceu = true;
      }
    });
  }

  void _changeQTDMinas(value) {
    print('change');
    print(value);
    print('state');
    _qtdBombas = value;
    print(_qtdBombas);
    _reiniciar();
  }

  Tabuleiro _getTabuleiro(double largura, double altura, int qtdeBombas) {
    if (_tabuleiro == null || _tabuleiro.qtdeBombas != qtdeBombas) {
      int qtdeColunas = 15;
      double tamanhoCampo = largura / qtdeColunas;
      int qtdeLinhas = (altura / tamanhoCampo).floor();
      print('gettabuleiro');
      print(_qtdBombas);

      _tabuleiro = Tabuleiro(
        linhas: qtdeLinhas,
        colunas: qtdeColunas,
        qtdeBombas: qtdeBombas,
      );
    }
    return _tabuleiro;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: ResultadoWidget(
          qtdBombas: _qtdBombas,
          venceu: _venceu,
          onReiniciar: _reiniciar,
          onChangeQTDMinas: _changeQTDMinas,
        ),
        body: Container(
          color: MyColors.ocean_blue,
          child: Padding(
            padding: const EdgeInsets.all(5.0),
            child: LayoutBuilder(
              builder: (ctx, constraints) {
                return TabuleiroWidget(
                  tabuleiro: _getTabuleiro(
                    constraints.maxWidth,
                    constraints.maxHeight,
                    _qtdBombas,
                  ),
                  onAbrir: _abrir,
                  onAlternarMarcacao: _alternarMarcacao,
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
