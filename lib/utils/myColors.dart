import 'package:flutter/material.dart';

class MyColors {
  static const MaterialColor ocean_blue = MaterialColor(
    0xFF5634CD,
    <int, Color>{
      50: Color(0xFF5634CD),
      100: Color(0xFF5634CD),
      200: Color(0xFF5634CD),
      300: Color(0xFF5634CD),
      400: Color(0xFF5634CD),
      500: Color(0xFF5634CD),
      600: Color(0xFF5634CD),
      700: Color(0xFF5634CD),
      800: Color(0xFF5634CD),
      900: Color(0xFF5634CD),
    },
  );

  static const MaterialColor opal = MaterialColor(
    0xFFBCD0D2,
    <int, Color>{
      50: Color(0xFFBCD0D2),
      100: Color(0xFFBCD0D2),
      200: Color(0xFFBCD0D2),
      300: Color(0xFFBCD0D2),
      400: Color(0xFFBCD0D2),
      500: Color(0xFFBCD0D2),
      600: Color(0xFFBCD0D2),
      700: Color(0xFFBCD0D2),
      800: Color(0xFFBCD0D2),
      900: Color(0xFFBCD0D2),
    },
  );

  static const MaterialColor cultured = MaterialColor(
    0xFFF8FAFA,
    <int, Color>{
      50: Color(0xFFF8FAFA),
      100: Color(0xFFF8FAFA),
      200: Color(0xFFF8FAFA),
      300: Color(0xFFF8FAFA),
      400: Color(0xFFF8FAFA),
      500: Color(0xFFF8FAFA),
      600: Color(0xFFF8FAFA),
      700: Color(0xFFF8FAFA),
      800: Color(0xFFF8FAFA),
      900: Color(0xFFF8FAFA),
    },
  );

  static const MaterialColor phthalo_blue = MaterialColor(
    0xFF1D0C8E,
    <int, Color>{
      50: Color(0xFF1D0C8E),
      100: Color(0xFF1D0C8E),
      200: Color(0xFF1D0C8E),
      300: Color(0xFF1D0C8E),
      400: Color(0xFF1D0C8E),
      500: Color(0xFF1D0C8E),
      600: Color(0xFF1D0C8E),
      700: Color(0xFF1D0C8E),
      800: Color(0xFF1D0C8E),
      900: Color(0xFF1D0C8E),
    },
  );

  static const MaterialColor ochre = MaterialColor(
    0xFFC77927,
    <int, Color>{
      50: Color(0xFFC77927),
      100: Color(0xFFC77927),
      200: Color(0xFFC77927),
      300: Color(0xFFC77927),
      400: Color(0xFFC77927),
      500: Color(0xFFC77927),
      600: Color(0xFFC77927),
      700: Color(0xFFC77927),
      800: Color(0xFFC77927),
      900: Color(0xFFC77927),
    },
  );

  static const MaterialColor red_salsa = MaterialColor(
    0xFFF94144,
    <int, Color>{
      50: Color(0xFFF94144),
      100: Color(0xFFF94144),
      200: Color(0xFFF94144),
      300: Color(0xFFF94144),
      400: Color(0xFFF94144),
      500: Color(0xFFF94144),
      600: Color(0xFFF94144),
      700: Color(0xFFF94144),
      800: Color(0xFFF94144),
      900: Color(0xFFF94144),
    },
  );

  static const MaterialColor pistachio = MaterialColor(
    0xFF90BE6D,
    <int, Color>{
      50: Color(0xFF90BE6D),
      100: Color(0xFF90BE6D),
      200: Color(0xFF90BE6D),
      300: Color(0xFF90BE6D),
      400: Color(0xFF90BE6D),
      500: Color(0xFF90BE6D),
      600: Color(0xFF90BE6D),
      700: Color(0xFF90BE6D),
      800: Color(0xFF90BE6D),
      900: Color(0xFF90BE6D),
    },
  );

  static const MaterialColor mauvelous = MaterialColor(
    0xFFE3A0B0,
    <int, Color>{
      50: Color(0xFFE3A0B0),
      100: Color(0xFFE3A0B0),
      200: Color(0xFFE3A0B0),
      300: Color(0xFFE3A0B0),
      400: Color(0xFFE3A0B0),
      500: Color(0xFFE3A0B0),
      600: Color(0xFFE3A0B0),
      700: Color(0xFFE3A0B0),
      800: Color(0xFFE3A0B0),
      900: Color(0xFFE3A0B0),
    },
  );

  static const MaterialColor rich_black = MaterialColor(
    0xFF0B1019,
    <int, Color>{
      50: Color(0xFF0B1019),
      100: Color(0xFF0B1019),
      200: Color(0xFF0B1019),
      300: Color(0xFF0B1019),
      400: Color(0xFF0B1019),
      500: Color(0xFF0B1019),
      600: Color(0xFF0B1019),
      700: Color(0xFF0B1019),
      800: Color(0xFF0B1019),
      900: Color(0xFF0B1019),
    },
  );
}
