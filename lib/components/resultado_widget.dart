import 'package:campominado/utils/myColors.dart';
import 'package:flutter/material.dart';

class ResultadoWidget extends StatelessWidget implements PreferredSizeWidget {
  final int qtdBombas;
  final bool venceu;
  final Function onReiniciar;
  final Function onChangeQTDMinas;

  ResultadoWidget({
    @required this.qtdBombas,
    @required this.venceu,
    @required this.onReiniciar,
    @required this.onChangeQTDMinas,
  });

  Color _getCor() {
    if (venceu == null) {
      return MyColors.cultured;
    } else if (venceu) {
      return MyColors.pistachio;
    } else {
      return MyColors.red_salsa;
    }
  }

  IconData _getIcon() {
    if (venceu == null) {
      return Icons.sentiment_satisfied;
    } else if (venceu) {
      return Icons.sentiment_very_satisfied;
    } else {
      return Icons.sentiment_very_dissatisfied;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: MyColors.ocean_blue,
      child: SafeArea(
        child: Container(
          padding: EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: Row(
                  children: [
                    DropdownButton<int>(
                      iconEnabledColor: MyColors.cultured,
                      style: TextStyle(
                        color: MyColors.cultured,
                      ),
                      value: qtdBombas,
                      dropdownColor: MyColors.ocean_blue,
                      icon: Icon(Icons.arrow_downward),
                      onChanged: onChangeQTDMinas,
                      items: <int>[3, 5, 10, 15, 20].map<DropdownMenuItem<int>>(
                        (e) {
                          return DropdownMenuItem(
                            value: e,
                            child: Text('$e Minas'),
                          );
                        },
                      ).toList(),
                      underline: Container(
                        height: 2,
                        color: MyColors.cultured,
                      ),
                    ),
                  ],
                ),
              ),
              CircleAvatar(
                backgroundColor: _getCor(),
                child: IconButton(
                  padding: EdgeInsets.all(0),
                  icon: Icon(
                    _getIcon(),
                    color: Colors.black,
                    size: 35,
                  ),
                  onPressed: onReiniciar,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(120);
}
